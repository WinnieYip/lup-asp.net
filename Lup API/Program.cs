using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace Lup_API
{
    public class Program
    {

        public static void Main(string[] args)
        {           
            using var con = new NpgsqlConnection("host=localhost;port=5432;database=lup;user id=postgres password=;");
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}