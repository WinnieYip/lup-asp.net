﻿using System.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;

namespace Lup_API
{
    public class EventContext : DbContext
    {
        public EventContext() : base(nameOrConnectionString: "Lup") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }

        // Expose the Event model
        public DbSet<Event> Event { get; set; }
    }
}