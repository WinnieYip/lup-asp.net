using System;
using System.ComponentModel.DataAnnotations;

namespace Lup_API
{
    public class Event
    {
        
        public Event()
        {
            this.Id = Guid.NewGuid();
        }
        
        [Key]
        public Guid Id { get; set; }
        
        [MaxLength(100)]
        [Required]
        public string Name { get; set; }

        [Required]
        [MaxLength(400)]
        public string Description { get; set; }
        
        public string Timezone { get; set; }
        
        [Required]
        public DateTime DateStart { get; set; }
        
        [Required]
        public DateTime DateEnd { get; set; }

        public DateTime DateCreatedUTC { get; set; }
        
        public DateTime DateModifiedUTC { get; set; }
    }
}