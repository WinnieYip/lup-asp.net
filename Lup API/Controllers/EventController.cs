﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Lup_API.Controllers
{
    [ApiController]
    [Route("events")]
    [Produces("application/json")]

    public class EventController : ControllerBase
    {
        /// <summary>
        /// Create Event.
        /// </summary>
        /// <returns>Creates and returns Event.</returns>   
        [HttpPost("Create")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> Create([FromBody] EventDTO dto)
        {
            try
            {
                var entity = dto.ToEntity();

                // Create new entity from DTO
                // Save changes

                return this.Ok();
            }

            catch (Exception e)
            {
                return null;
            }

        }

        /// <summary>
        /// Get all Events.
        /// </summary>
        /// <returns>Returns all Events.</returns>
        [HttpGet("all")]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(EventDTO[]))]
        public async Task<IActionResult> GetAllEvents()
        {
            var eventDtos = await this.GetAllAsync();

            return this.Ok(eventDtos);
        }

        /// <summary>
        /// Deletes an Event.
        /// </summary>
        /// <returns>Deletes an event from the given guid.</returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deleted = await this.Event.DeleteAsync(id);
            if (!deleted)
            {
                return this.NotFound();
            }

            return this.Ok();
        }

        /// <summary>
        /// Updates an Event.
        /// </summary>
        /// <returns>Updates an event from the given model.</returns>
        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(EventDTO))]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Put([FromBody] EventDTO dto)
        {
            var updatedEvent = await this.Event.UpdateAsync(dto);
            if (updatedEvent == null)
            {
                return this.BadRequest();
            }

            return this.Ok(updatedEvent);
        }
    }
}