﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Lup_API
{
    public class EventDTO
    {
        public EventDTO()
        {
            this.Id = Guid.NewGuid();
        }

        [Key] public Guid Id { get; set; }

        [Required] public string Name { get; set; }

        [Required] public string Description { get; set; }

        public string Timezone { get; set; }

        [Required] public DateTime DateStart { get; set; }

        [Required] public DateTime DateEnd { get; set; }
    }
}